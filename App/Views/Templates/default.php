<!DOCTYPE html>
<html lang="fr-FR">
  <head>
    <title>{% block title %}{% endblock %}</title>    
      
  <!-- SEO -->
    <!-- META DESCRIPTION --> 
    <meta charset="UTF-8">
    <meta name="description" content="Réseau social professionel pour l'ECE">
    <meta name="keywords" content="Réseau social, professionel, ECE Paris, Emploi">
    <meta name="author" content="Adrien Boppe, Bastien Pederencino et Léo Fritz">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- OPEN GRAPH -->
    <meta property="og:locale" content="fr_FR">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Paradoxe">
    <meta property="og:url" content="https://paradoxe.xyz">

    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">

    <!-- TWITTER CARD -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@BPederencino">
    <meta name="twitter:creator" content="@BPederencino">

    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
  <!-- END OF SEO -->

  <!-- LINKS -->        
    <!-- FONTS -->
    <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">-->
              
    <!-- STYLESHEETS -->
    <link rel="stylesheet" href="/public/styles/style.css">
              
    <!-- FAVICON -->
    <!--<link rel="shortcut icon" type="image/png" href="/public/graphics/logos/favicon.png">-->
              
    <!-- CANONICAL URL TAG -->
    <link rel="canonical" href="https://paradoxe.xyz">            
  <!-- END OF LINKS -->  

  <!-- SCRIPTS -->        
    <!-- CUSTOM -->
    <script src="/public/scripts/custom.js"></script>
    <noscript>Votre navigateur web ne supporte pas JavaScript.</noscript>       
  <!-- END OF SCRIPTS -->
  </head>
  <body>
      
    <header id="header" class="header" role="banner">
        {% block header %}
        {% endblock %}
    </header>
          
    <main id="main" class="main" role="main">
        {% block main %}
        {% endblock %}
    </main>
          
    <footer id="footer" class="footer" role="contentinfo">
        {% block footer %}
        {% endblock %}
    </footer>
    
      {% block endofbody %}
      {% endblock %}
  </body>
</html>
