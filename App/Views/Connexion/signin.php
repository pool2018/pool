{% extends "Templates/default.php" %}

{% block title %}Connexion{% endblock %}

{% block header %}

    <img src="/public/graphics/logos/logo.png" alt="Logo" />
    <nav>
      <ul>
        <li><a href="/message">Messagerie</a></li>
        <li><a href="/profile">Profil</a></li>
        <li><a href="/network">Réseau</a></li>
        <li><a href="/signin">Connexion</a></li>
        <li><a href="/register">Inscription</a></li>
      </ul>
    </nav>

{% endblock %}

{% block main %}

  <div class="row">
      <div class="col-4">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
      <div class="col-4">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
            <input type="text" />
          </form>
        </div>
      </div>
      <div class="col-4">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
  </div>
  
  <div class="row">
      <div class="col-3">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
      <div class="col-3">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
      <div class="col-3">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
      <div class="col-3">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
  </div>

{% endblock %}

{% block footer %}

    <p>Copyleft</p>

{% endblock %}
