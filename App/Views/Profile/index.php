{% extends "Templates/default.php" %}

{% block title %}Profil{% endblock %}

{% block header %}

    <img src="/public/graphics/logos/logo.png" alt="Logo" />
    <nav>
      <ul>
        <li><a href="/message">Messagerie</a></li>
        <li><a href="/profile">Profil</a></li>
        <li><a href="/network">Réseau</a></li>
        <li><a href="/signin">Connexion</a></li>
        <li><a href="/register">Inscription</a></li>
      </ul>
    </nav>

{% endblock %}

{% block main %}

  <div class="row">
      <div class="col-4">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          
              <!-- Le type d'encodage des données, enctype, DOIT être spécifié comme ce qui suit -->
              <form enctype="multipart/form-data" action="_URL_" method="post">
                <!-- MAX_FILE_SIZE doit précéder le champ input de type file -->
                <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
                <!-- Le nom de l'élément input détermine le nom dans le tableau $_FILES -->
                Envoyez ce fichier : <input name="userfile" type="file" />
                <input type="submit" value="Envoyer le fichier" />
              </form>
          
        </div>
      </div>
      <div class="col-4">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
            <input type="text" />
          </form>
        </div>
      </div>
      <div class="col-4">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
  </div>
  
  <div class="row">
      <div class="col-3">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
      <div class="col-3">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
      <div class="col-3">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
      <div class="col-3">
        <div class="cell shadow--2dp">
          <h1>Hello world</h1>
          <p>Lorem ipsu si amet</p>
          <form>
            <input type="text" />
          </form>
        </div>
      </div>
  </div>

{% endblock %}

{% block footer %}

    <p>Copyleft</p>

{% endblock %}
