{% extends "Templates/message.php" %}

{% block title %}Messagerie{% endblock %}

{% block header %}

    <img src="/public/graphics/logos/logo.png" alt="Logo" />
    <nav>
      <ul>
        <li><a href="/message">Messagerie</a></li>
        <li><a href="/profile">Profil</a></li>
        <li><a href="/network">Réseau</a></li>
        <li><a href="/signin">Connexion</a></li>
        <li><a href="/register">Inscription</a></li>
      </ul>
    </nav>

{% endblock %}

{% block main %}



  <div class="row">
      <div class="col-12">
        <div class="cell shadow--2dp">
        
          <div id="container">
          
	          <h1>Messagerie</h1>
	          
            <table class="chat">
              <tr>		
	              <!-- Message area -->
	              <td valign="top" id="text-td">
                  <div id="annonce">
                    <h4>{pseudos des participants}</h4>
                  </div>
		              <div id="text">
	                  <div id="loading">
		                  <center>
	              	      <span class="info" id="info">Chargement du chat en cours...</span><br />
			                  <img src="/public/graphics/elements/ajax-loader.gif" alt="patientez...">
			                </center>
		                </div>
	                </div>
                </td>
              </tr>
            </table>

            <!-- Text area -->
            <a name="post"></a>
            <table class="post_message">
              <tr>
		            <td>
		              <form action="" method="" onsubmit="envoyer(); return false;">
			              <input type="text" id="message" maxlength="255" />
			              <input type="button" onclick="envoyer()" value="Envoyer" id="post" />
		              </form>
                  <div id="responsePost" style="display:none"></div>
		            </td>
	            </tr>
            </table>

	        </div>
          
        </div>
      </div>
  </div>



{% endblock %}

{% block footer %}

    <p>Copyleft</p>

{% endblock %}
