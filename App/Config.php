<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'paradoxeryroot.mysql.db';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'paradoxeryroot';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'paradoxeryroot';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = 'Pool2018SQL';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;
}
