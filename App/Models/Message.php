<?php

namespace App\Models;

use PDO;

/**
 * Message model
 *
 * PHP version 7.0
 */
class Message extends \Core\Model
{


    /**
     * Transform URL to links in message
     *
     * @return content
     */
    public function urllink($content='') {
	          $content = preg_replace('#(((https?://)|(w{3}\.))+[a-zA-Z0-9&;\#\.\?=_/-]+\.([a-z]{2,4})([a-zA-Z0-9&;\#\.\?=_/-]+))#i', '<a href="$0" target="_blank">$0</a>', $content);
	          
	          // Add http:// to links
	          if(preg_match('#<a href="www\.(.+)" target="_blank">(.+)<\/a>#i', $content)) {
		          $content = preg_replace('#<a href="www\.(.+)" target="_blank">(.+)<\/a>#i', '<a href="http://www.$1" target="_blank">www.$1</a>', $content);
		          //preg_replace('#<a href="www\.(.+)">#i', '<a href="http://$0">$0</a>', $content);
	          }

	          $content = stripslashes($content);
	          return $content;
    }
    
      
    /**
     * Get all the message from a conversation as JSON
     *
     * @return JSON
     */
    public static function getMessage()
    {
        $db = static::getDB();

        $query = $db->prepare("
	        SELECT message_id, message_user, message_time, message_text, account_id, account_login
	        FROM chat_messages
	        LEFT JOIN chat_accounts ON chat_accounts.account_id = chat_messages.message_user
	        WHERE message_time >= :time
	        ORDER BY message_time ASC LIMIT 0,100
        ");
        
        $query->execute(array(
	        'time' => $_GET['dateConnexion']
        ));
        
        $count = $query->rowCount();
        
        if($count != 0) {
	        $json['messages'] = '<div id="messages_content">';
	        $json['messages'] .= '<table><tr><td style="height:500px;" valign="bottom">';
	        $json['messages'] .= '<table style="width:100%">';

	        $i = 1;
	        $e = 0;
	        $prev = 0;
	        
	        while ($data = $query->fetch()) {
		        // Change color to different users 
		        if($i != 1) {
			        $idNew = $data['message_user'];		
			        if($idNew != $id) {
				        if($colId == 1) {
					        $color = '#077692';
					        $colId = 0;
				        } else {
					        $color = '#666';
					        $colId = 1;
				        }
				        $id = $idNew;
			        } else
				        $color = $color;
		        } else {
			        $color = '#666';
			        $id = $data['message_user'];
			        $colId = 1;
		        }


		        $text .= '<tr><td style="width:15%" valign="top">';

		        if($prev != $data['account_id']) {
			        // Message content	
			        $text .= '<a href="#post" onclick="insertLogin(\''.addslashes($data['account_login']).'\')" style="color:black">';
			        $text .= date('[H:i]', $data['message_time']);
			        $text .= '&nbsp;<span style="color:'.$color.'">'.$data['account_login'].'</span>';
			        $text .= '</a>';	
		        }
		        $text .= '</td>';			
		        $text .= '<td style="width:85%;padding-left:10px;" valign="top">';

			        
		        $message = htmlspecialchars($data['message_text']); 

		        $message = urllink($message);
			        
		        if(user_verified()){
			        if(preg_match('#'.$_SESSION['login'].'&gt;#is', $message)) {
				        $message = preg_replace('#'.$_SESSION['login'].'&gt;#is', '<b><span style="color:orange;">'.$_SESSION['login'].'&gt;</span></b>', $message);
			        }
		        }
			        
		        $text .= $message.'<br />';
		        $text .= '</td></tr>';

		        $i++;
		        $prev = $data['account_id'];
	        }
	        
		        
	        // Message column in JSON with all messages 
	        
	        $json['messages'] = $text;

	        $json['messages'] .= '</table>';
	        $json['messages'] .= '</td></tr></table>';
	        $json['messages'] .= '</div>';			
        } else {
	        $json['messages'] = 'Aucun message n\'a été envoyé pour le moment.';
        }
        $query->closeCursor();
        
        return json_encode($json);
    }
    
    
     
    /**
     * Post a new message to a conversation
     *
     * @return void
     */
     
    public static function postMessage()
    {
        $db = static::getDB();
    
        if(isset($_POST['message']) AND !empty($_POST['message'])) {	

		        if(!preg_match("#^[-. ]+$#", $_POST['message'])) {
		        	
			         $query = $db->prepare("SELECT * FROM chat_messages WHERE message_user = :user ORDER BY message_time DESC LIMIT 0,1");
			         
			         $query->execute(array(
			  	        'user' => $_SESSION['id']
			          ));
			          
	  		        $count = $query->rowCount();
		  	        $data = $query->fetch();
			          // Check DOUBLON
			         if($count != 0)
				          similar_text($data['message_text'], $_POST['message'], $percent);
  
			         if($percent < 80) {
			  	        // Check timestamp
			  	        if(time()-5 >= $data['message_time']) {

					            
                    $insert = $db->prepare('
	                    INSERT INTO chat_messages (message_id, message_user, message_time, message_text) 
	                    VALUES(:id, :user, :time, :text)
                    ');
                    $insert->execute(array(
	                    'id' => '',
	                    'user' => $_SESSION['id'],
	                    'time' => time(),
	                    'text' => $_POST['message']
                    ));
                    
                    return true;


				        } else
			  		        echo 'Votre dernier message est trop récent. Baissez le rythme :D';	
			        } else
			  	        echo 'Votre dernier message est très similaire.';	
		        } else
			          echo 'Votre message est vide.';	
	        } else
		          echo 'Votre message est vide.';	
        } 
        
}
