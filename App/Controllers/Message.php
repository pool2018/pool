<?php

namespace App\Controllers;

use \App\Models;
use \Core\View;

/**
 * Message controller
 */
class Message extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        View::renderTemplate('Message/index.php', [
            'name'    => \App\Models\Message::getMessage()
        ]);
    }
}
