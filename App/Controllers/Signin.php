<?php

namespace App\Controllers;

use \Core\View;

/**
 * Connexion controller
 */
class Connexion extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function signinAction()
    {
        View::renderTemplate('Connexion/signin.php');
    }
    public function registerAction()
    {
        View::renderTemplate('Connexion/register.php');
    }
}
