<?php

namespace App\Controllers;

use \Core\View;

/**
 * Network controller
 */
class Network extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        View::renderTemplate('Network/index.php');
    }
}
