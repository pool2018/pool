<?php

namespace App\Controllers;

use \Core\View;

/**
 * Helloworld controller
 */
class Helloworld extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        View::renderTemplate('Helloworld/index.php');
    }
}
