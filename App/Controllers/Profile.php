<?php

namespace App\Controllers;

use \Core\View;

/**
 * Profile controller
 */
class Profile extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        View::renderTemplate('Profile/index.php');
    }
}
