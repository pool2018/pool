<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');


/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('{controller}/{action}');

$router->add('helloworld', ['controller' => 'Helloworld', 'action' => 'index']);
$router->add('message', ['controller' => 'Message', 'action' => 'index']);
$router->add('network', ['controller' => 'Network', 'action' => 'index']);
$router->add('signin', ['controller' => 'Connexion', 'action' => 'signin']);
$router->add('register', ['controller' => 'Connexion', 'action' => 'register']);
$router->add('profile', ['controller' => 'Profile', 'action' => 'index']);
    
$router->dispatch($_SERVER['QUERY_STRING']);
