var reloadTime = 1000;
var scrollBar = false;

function getMessages() {

	$.getJSON('phpscripts/get-message.php?dateConnexion='+$("#dateConnexion").val(), function(data) {

			if(data['error'] == '0') {

				var container = $('#text');
  			var content = $('#messages_content');
				var height = content.height()-500;
				var toBottom;

				if(container[0].scrollTop == height)
					toBottom = true;
				else
					toBottom = false;


				$("#annonce").html('<span class="info"><b>'+data['annonce']+'</b></span><br /><br />');
				$("#text").html(data['messages']);


  				content = $('#messages_content');
				height = content.height()-500;
				
				if(toBottom == true)
					container[0].scrollTop = content.height();	
  
   				if(scrollBar != true) {
					container[0].scrollTop = content.height();
					scrollBar = true;
				}	
			} else if(data['error'] == 'unlog') {

				$("#annonce").html('');
				$("#text").html('');
				$(location).attr('href',"chat.php");
			}
	});
}




function postMessage() {

	var message = encodeURIComponent($("#message").val());
	$.ajax({
		type: "POST",
		url: "phpscripts/post-message.php",
		data: "message="+message,
		success: function(msg){

			if(msg == true) {

				$("#message").val('');
				$("#responsePost").slideUp("slow").html('');
			} else
				$("#responsePost").html(msg).slideDown("slow");

			$("#message").focus();
		},
		error: function(msg){

			alert('Erreur');
		}
	});
}



$(document).ready(function() {

	if(document.getElementById('message')) {
		window.setInterval(getMessages, reloadTime);
		$("#message").focus();
	}
});
